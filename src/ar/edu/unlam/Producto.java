package ar.edu.unlam;

public class Producto {

	private String nombre;
	private Double precio;
	private Integer codigo;

	public Producto(String nombre, Double precio, Integer codigo) {
		this.nombre = nombre;
		this.precio = precio;
		this.codigo = codigo;
	}
}
